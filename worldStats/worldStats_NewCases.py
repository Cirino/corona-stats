from datetime import datetime, timedelta
import pandas as pd
from plotMap_world import map_world

path = "files/"

csvRead_popWorld = pd.read_csv(path + "popWorld_moreStats.csv", delimiter=",")
csvRead_dataCovid = pd.read_csv(path + "data_covid_world.csv", delimiter=",")

popCountrys = csvRead_popWorld.loc[:, ["Pop2020", "country"]]

# Calcular passagem dos dias
maxDate = csvRead_dataCovid.date.max()
minDate = csvRead_dataCovid.date.min()
# minDate = "2020-05-15"
date = datetime.strptime(minDate, "%Y-%m-%d").date()
day = date + timedelta(days=1)
dayMore, milion = 0, 10 ** 6
while str(maxDate) != str(day):
    dict_cases = {}
    dateCovid = csvRead_dataCovid.loc[csvRead_dataCovid["date"] == str(day)]
    caseCountrys = dateCovid.loc[:, ["new_cases"]]

    for c in dateCovid.location:
        if c not in ["World", "International"]:
            countryNewCases = dateCovid.loc[dateCovid["location"] == c].new_cases.values
            iso_code = dateCovid.loc[dateCovid["location"] == c].iso_code.values
            countryTotalPop = popCountrys.loc[
                popCountrys["country"] == c
            ].Pop2020.values

            # Ajeita a variável para ficar no formato correto
            dailyNewCases = countryNewCases[0]

            dict_cases[iso_code[0]] = float(dailyNewCases)

    map_world(dict_cases, str(day), "NewCases/", "New cases per day")

    day = date + timedelta(days=dayMore)
    dayMore += 1

print("ok")
