import pandas as pd
import seaborn as sns
from datetime import datetime, timedelta
import matplotlib.pyplot as plt


path = "files/"

csvRead_popWorld = pd.read_csv(path + "popWorld_moreStats.csv", delimiter=",")
csvRead_dataCovid = pd.read_csv(path + "data_covid_world.csv", delimiter=",")

popCountrys = csvRead_popWorld.loc[
    :, ["Pop2020", "country", "density", "med_age", "urban_pop"]
]

# Calcular passagem dos dias
maxDate = csvRead_dataCovid.date.max()

dateCovid = csvRead_dataCovid.loc[csvRead_dataCovid["date"] == str(maxDate)]
caseCountrys = dateCovid.loc[:, ["total_deaths"]]
dayMore, milion = 0, 10 ** 6

list_cases, list_density, list_medAge, list_urban = [], [], [], []
for c in dateCovid.location:
    if c not in ["World", "International"]:
        countryTotalCases = dateCovid.loc[
            dateCovid["location"] == c
        ].total_deaths.values

        iso_code = dateCovid.loc[dateCovid["location"] == c].iso_code.values
        countryDensity = popCountrys.loc[popCountrys["country"] == c].density.values
        country_medAge = popCountrys.loc[popCountrys["country"] == c].med_age.values
        country_urban = popCountrys.loc[popCountrys["country"] == c].urban_pop.values

        countryTotalPop = popCountrys.loc[popCountrys["country"] == c].Pop2020.values
        print(countryTotalPop)
        if countryTotalPop != " ":
            countryTotalPop = int(countryTotalPop[0].replace(",", ""))
            countryDensity = int(countryDensity[0].replace(",", ""))
            if countryTotalPop > (10 * (10 ** 6)):
                casesPerMilion = float(
                    int(countryTotalCases[0]) / (int(countryTotalPop) / milion)
                )
            elif countryTotalPop > (10 ** 6):
                casesPerMilion = float(
                    float(countryTotalCases[0]) / (int(countryTotalPop) / (10 ** 3))
                )
            else:
                casesPerMilion = countryTotalCases[0]

            list_cases.append(casesPerMilion)
            list_density.append(
                int(1 ** 3 if countryDensity > 1000 else countryDensity)
            )

            list_medAge.append(int(country_medAge))

            list_urban.append(int(country_urban[0].split("%")[0]))


list_cases = pd.DataFrame(list_cases)
list_density = pd.DataFrame(list_density)
list_medAge = pd.DataFrame(list_medAge)
list_urban = pd.DataFrame(list_urban)


sns.regplot(x=list_cases, y=list_density)
plt.xlabel("Death per milion")
plt.ylabel("Density(pop/km²)")
plt.title("Distribution of the number of deaths according to density")
plt.savefig("files/Plots/density.png", format="png")
plt.close()

sns.regplot(x=list_cases, y=list_medAge)
plt.xlabel("Death per milion")
plt.ylabel("Mean age")
plt.title("Distribution of the number of deaths according to mean age")
plt.savefig("files/Plots/medAge.png", format="png")
plt.close()

sns.regplot(x=list_cases, y=list_urban)
plt.xlabel("Death per milion")
plt.ylabel("Urban pop(%)")
plt.title("Distribution of the number of deaths according to urban pop")
plt.savefig("files/Plots/urbanPOP.png", format="png")
plt.close()
