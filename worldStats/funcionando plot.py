import geopandas
import numpy
import pandas as pd
import matplotlib.pyplot as plt
import json
import random
import time

world = geopandas.read_file(geopandas.datasets.get_path("naturalearth_lowres"))

world = world[(world.pop_est > 0) & (world.name != "China")]

list_serie, list_index, i, dict_countryID = [], [], 0, {}
dict_test = {}
for country in world["name"]:
    dict_test["random"] = random.randint(0, 10)
    list_serie.append(dict_test)
    dict_test = {}

world["teste"] = pd.DataFrame(list_serie)

world.plot(
    column="teste",
    legend=True,
    legend_kwds={"label": "Random", "orientation": "horizontal"},
)

print(plt)
