import geopandas
import numpy
import time
import pandas as pd
import matplotlib.pyplot as plt


def map_world(dictPlot, date, past, title):
    world = geopandas.read_file(geopandas.datasets.get_path("naturalearth_lowres"))
    world = world[world.name != "Antarctica"]

    dict_serie, list_serie = {}, []
    for code in world["iso_a3"]:
        if code in dictPlot:
            dict_serie["cases"] = dictPlot[str(code)]
        else:
            dict_serie["cases"] = 0
        list_serie.append(dict_serie)
        dict_serie = {}

    world["cases"] = pd.DataFrame(list_serie)
    world.plot(
        column="cases",
        legend=True,
        legend_kwds={"label": title + ": " + date, "orientation": "horizontal"},
        cmap="Blues",
    )

    plt.savefig("files/Plots/" + past + date + ".png", format="png")
