import pandas as pd
from datetime import datetime, timedelta
import matplotlib.pyplot as plt


path = "files/"


def arruma_data(csvRead):
    data = csvRead.Data.values
    dict_NewData = {}
    data_a3 = {
        "jan": "01",
        "fev": "02",
        "mar": "03",
        "abr": "04",
        "mai": "05",
        "jun": "06",
        "jul": "07",
        "ago": "08",
        "set": "09",
        "out": "10",
        "nov": "11",
        "dez": "12",
    }

    i = 0
    for date in data:
        list_DateSplit = date.split(" ")

        list_DateSplit[1] = data_a3[list_DateSplit[1]]
        dict_NewData["2020-" + list_DateSplit[1] + "-" + list_DateSplit[0]] = i
        i += 1

    return dict_NewData


csvRead_Sp = pd.read_csv(path + "covid-19-estado-SP-PorDia.csv", delimiter="\t")

csvRead_Sp["date"] = arruma_data(csvRead_Sp)
maxDate = csvRead_Sp.date.max()
minDate = csvRead_Sp.date.min()
date = datetime.strptime(minDate, "%Y-%m-%d").date()

day, dayMore = date, 0
list_date, list_case = [], []
while str(maxDate) != str(day):
    casesPerDay = csvRead_Sp.loc[csvRead_Sp["date"] == str(day)].Total_de_casos.values

    dayNew = str(day).split("-")
    list_date.append(dayNew[1] + "/" + dayNew[2])
    list_case.append(casesPerDay[0])

    day = date + timedelta(days=dayMore)
    dayMore += 1
print(list_date, list_case)
dfCases = pd.DataFrame({"x": list_date, "y1": list_case})
plt.plot(
    "x",
    "y1",
    data=dfCases,
    marker="o",
    markerfacecolor="blue",
    markersize=5,
    color="skyblue",
    linewidth=2,
)

plt.show()
